#!/usr/bin/env python3

"""The cutcutcodec preferences."""

from .config import Config

__all__ = ["Config"]
