/* Fast image metric. */

#define PY_SSIZE_T_CLEAN
// for detection and management of nestead threads
#include <unistd.h>
#include <sys/syscall.h>
#ifndef SYS_gettid
#error 'SYS_gettid unavailable on this system'
#endif
#define gettid() ((pid_t)syscall(SYS_gettid))
// classical imports
#include <complex.h>
#include <math.h>
#include <numpy/arrayobject.h>
#include <omp.h>
#include <Python.h>
#include <stdlib.h>


int compute_mse_float32(double* mse, PyArrayObject* im1, PyArrayObject* im2, PyArrayObject* weights) {
  /* Compute the mse for each channel, return the ponderated average. */
  float local_mse = 0.0;  // we can not declare *mse in reduction
  #pragma omp parallel for simd schedule(static) collapse(2) reduction(+:local_mse)
  for ( npy_intp i = 0; i < PyArray_DIM(im1, 0); ++i ) {
    for ( npy_intp j = 0; j < PyArray_DIM(im1, 1); ++j ) {
      for ( npy_intp k = 0; k < PyArray_DIM(im1, 2); ++k ) {
        float diff = *(float *)PyArray_GETPTR3(im1, i, j, k) - *(float *)PyArray_GETPTR3(im2, i, j, k);
        diff *= diff;
        diff *= (float)(  // we can't factorise because omp doesn't support array reduction
          *(double *)PyArray_GETPTR1(weights, k)
        );
        local_mse += diff;  // critical for thread safe
      }
    }
  }
  local_mse /= (float)PyArray_DIM(im1, 0) * (float)PyArray_DIM(im1, 1);
  *mse = (double)local_mse;
  return EXIT_SUCCESS;
}


int compute_mse_float64(double* mse, PyArrayObject* im1, PyArrayObject* im2, PyArrayObject* weights) {
  /* Compute the mse for each channel, return the ponderated average. */
  double local_mse = 0.0;  // we can not declare *mse in reduction
  #pragma omp parallel for schedule(static) collapse(2) reduction(+:local_mse)
  for ( npy_intp i = 0; i < PyArray_DIM(im1, 0); ++i ) {
    for ( npy_intp j = 0; j < PyArray_DIM(im1, 1); ++j ) {
      for ( npy_intp k = 0; k < PyArray_DIM(im1, 2); ++k ) {
        double diff = *(double *)PyArray_GETPTR3(im1, i, j, k) - *(double *)PyArray_GETPTR3(im2, i, j, k);
        diff *= diff;
        diff *= *(double *)PyArray_GETPTR1(weights, k);  // we can't factorise because omp doesn't support array reduction
        local_mse += diff;  // critical for thread safe
      }
    }
  }
  local_mse /= (double)PyArray_DIM(im1, 0) * (double)PyArray_DIM(im1, 1);
  *mse = local_mse;
  return EXIT_SUCCESS;
}


PyArrayObject* gauss_kernel(npy_intp radius, double sigma) {
  /* Create a gaussian kernel. */
  npy_intp shape[2] = {2*radius + 1, 2*radius + 1};
  PyArrayObject* gauss2d;
  double* gauss1d;
  double sum, buff;
  // verifiactions
  if ( radius < 1 ) {
    PyErr_SetString(PyExc_ValueError, "the gaussian radius must be >= 1");
    return NULL;
  }
  if ( sigma <= 0.0 ) {
    PyErr_SetString(PyExc_ValueError, "the variance has to be strictely positive");
    return NULL;
  }
  // allocations
  gauss1d = (double *)malloc((2 * radius + 1) * sizeof(double));
  if ( gauss1d == NULL ) {
    PyErr_NoMemory();
    return NULL;
  }
  gauss2d = (PyArrayObject *)PyArray_EMPTY(2, shape, NPY_DOUBLE, 0);
  if ( gauss2d == NULL ) {
    free(gauss1d);
    PyErr_NoMemory();
    return NULL;
  }

  // compute gaussian
  Py_BEGIN_ALLOW_THREADS
  buff = -1.0 / (2.0 * sigma * sigma);
  #pragma omp simd
  for ( npy_intp i = 1; i < radius + 1; ++i ) {  // compute gaussian 1d
    gauss1d[radius-i] = gauss1d[radius+i] = exp((double)(i*i) * buff);
  }
  gauss1d[radius] = 1.0;
  sum = 0.0;  // compute gaussian 2d
  #pragma omp simd collapse(2) reduction(+:sum)
  for ( npy_intp i = 0; i < shape[0]; ++i ) {
    for ( npy_intp j = 0; j < shape[0]; ++j ) {
      buff = gauss1d[i] * gauss1d[j];
      *(double *)PyArray_GETPTR2(gauss2d, i, j) = buff;
      sum += buff;
    }
  }
  sum = 1.0 / sum;  // normalise
  #pragma omp simd collapse(2)
  for ( npy_intp i = 0; i < shape[0]; ++i ) {
    for ( npy_intp j = 0; j < shape[0]; ++j ) {
      *(double *)PyArray_GETPTR2(gauss2d, i, j) *= sum;
    }
  }
  free(gauss1d);
  Py_END_ALLOW_THREADS
  return gauss2d;
}


double compute_patch_ssim_float64(
  PyArrayObject* im1,
  PyArrayObject* im2,
  npy_intp i0,
  npy_intp j0,
  npy_intp k,
  PyArrayObject* kernel  // sum of all elements is 1
) {
  // https://github.com/scikit-image/scikit-image/blob/v0.25.2/skimage/metrics/_structural_similarity.py
  // https://github.com/VainF/pytorch-msssim/blob/master/pytorch_msssim/ssim.py

  npy_intp radius[2] = {PyArray_DIM(kernel, 0) / 2, PyArray_DIM(kernel, 1) / 2};
  double mu1, mu2, s11, s22, s12, x1, x2, x1w, x2w, weight;
  double c1 = (0.01*1) * (0.01*1);
  double c2 = (0.03*1) * (0.03*1);
  npy_intp shift[2] = {i0-radius[0], j0-radius[1]};
  // compute mu1, mu2, s11, s22 and s12
  mu1 = 0.0, mu2 = 0.0, s11 = 0.0, s22 = 0.0, s12 = 0.0;
  for ( npy_intp i = 0; i < PyArray_DIM(kernel, 0); ++i ) {
    for ( npy_intp j = 0; j < PyArray_DIM(kernel, 1); ++j ) {
      weight = *(double *)PyArray_GETPTR2(kernel, i, j);
      x1 = *(double *)PyArray_GETPTR3(im1, i+shift[0], j+shift[1], k),
      x2 = *(double *)PyArray_GETPTR3(im2, i+shift[0], j+shift[1], k);
      x1w = x1 * weight, x2w = x2 * weight;
      mu1 += x1w, mu2 += x2w;
      s11 += x1 * x1w, s22 += x2 * x2w, s12 += x1 * x2w;
    }
  }
  s11 -= mu1 * mu1, s22 -= mu2 * mu2, s12 -= mu1 * mu2;
  // apply ssim formula
  return (
    (2.0 * mu1 * mu2 + c1) * (2.0 * s12 + c2)
  ) / (
    (mu1 * mu1 + mu2 * mu2 + c1) * (s11 + s22 + c2)
  );
}


int compute_ssim_float64(
  double* ssim,
  PyArrayObject* im1,
  PyArrayObject* im2,
  PyArrayObject* weights,
  PyArrayObject* kernel
) {
  npy_intp radius[2] = {PyArray_DIM(kernel, 0) / 2, PyArray_DIM(kernel, 1) / 2};  // rigorously (s - 1) / 2
  double local_ssim = 0.0;
  #pragma omp parallel for schedule(static) collapse(3) reduction(+:local_ssim)
  for ( npy_intp i = radius[0]; i < PyArray_DIM(im1, 0)-radius[0]; ++i ) {
    for ( npy_intp j = radius[1]; j < PyArray_DIM(im1, 1)-radius[1]; ++j ) {
      for ( npy_intp k = 0; k < PyArray_DIM(im1, 2); ++k ) {
        double patched_ssim = compute_patch_ssim_float64(im1, im2, i, j, k, kernel);
        patched_ssim *= *(double *)PyArray_GETPTR1(weights, k);
        local_ssim += patched_ssim;
      }
    }
  }
  local_ssim /= (double)((PyArray_DIM(im1, 0) - 2*radius[0]) * (PyArray_DIM(im1, 1) - 2*radius[1]));
  *ssim = local_ssim;
  return EXIT_SUCCESS;
}


int parse_float_array(PyObject *obj, void *addr) {
  /* Convert a pyobject sequence into an array of double. */
  if (!PySequence_Check(obj)) {
    PyErr_SetString(PyExc_TypeError, "it is expected to be a sequence");
    return 0;
  }

  npy_intp dims[1] = {(npy_intp)PySequence_Length(obj)};
  PyArrayObject* array = (PyArrayObject *)PyArray_EMPTY(1, dims, NPY_DOUBLE, 0);
  if ( array == NULL ) {
    PyErr_NoMemory();
    return 0;
  }

  for ( npy_intp i = 0; i < PyArray_DIM(array, 0); ++i ) {
    PyObject *item = PySequence_GetItem(obj, (Py_ssize_t)i);
    *(double *)PyArray_GETPTR1(array, i) = PyFloat_AsDouble(item);  // try to convert in float
    if ( PyErr_Occurred() ) {
      Py_DECREF(array);
      return 0;
    }
    Py_DECREF(item);
  }

  *(PyArrayObject **)addr = array;
  return 1;
}


static PyObject* py_ssim(PyObject* self, PyObject* args, PyObject* kwargs) {
  // declaration
  static char *kwlist[] = {"im1", "im2", "weights", "threads", NULL};
  PyArrayObject *im1, *im2, *weights = NULL;
  double ssim;
  long int threads = 0;
  int error = EXIT_SUCCESS;

  // parse and check
  if ( !PyArg_ParseTupleAndKeywords(
    args, kwargs, "O!O!|O&$l", kwlist,
    &PyArray_Type, &im1, &PyArray_Type, &im2, &parse_float_array, &weights, &threads
    )
  ) {
    return NULL;
  }
  if ( PyArray_NDIM(im1) != 3 ) {
    PyErr_SetString(PyExc_ValueError, "'im1' requires 3 dimensions");
    return NULL;
  }
  if ( PyArray_NDIM(im2) != 3 ) {
    PyErr_SetString(PyExc_ValueError, "'im2' requires 3 dimensions");
    return NULL;
  }
  if ( PyArray_DIM(im1, 0) != PyArray_DIM(im2, 0) ) {
    PyErr_SetString(PyExc_ValueError, "'im1' and 'im2' must have the same height");
    return NULL;
  }
  if ( PyArray_DIM(im1, 1) != PyArray_DIM(im2, 1) ) {
    PyErr_SetString(PyExc_ValueError, "'im1' and 'im2' must have the same width");
    return NULL;
  }
  if ( PyArray_DIM(im1, 2) != PyArray_DIM(im2, 2) ) {
    PyErr_SetString(PyExc_ValueError, "'im1' and 'im2' must have the same channels");
    return NULL;
  }
  if ( PyArray_TYPE(im1) != PyArray_TYPE(im2) ) {
    PyErr_SetString(PyExc_TypeError, "'im1' and 'im2' are not the same type");
    return NULL;
  }

  // default values
  if ( weights == NULL ) {
    npy_intp dims[1] = {PyArray_DIM(im1, 2)};
    weights = (PyArrayObject *)PyArray_EMPTY(1, dims, NPY_DOUBLE, 0);
    if ( weights == NULL ) {
      PyErr_NoMemory();
      return NULL;
    }
    #pragma omp simd
    for ( npy_intp i = 0; i < PyArray_DIM(weights, 0); ++i ) {
      *(double *)PyArray_GETPTR1(weights, i) = 1.0;
    }
  }

  // set omp nbr threads
  if ( threads == 0 ) {
    if ( gettid() == getpid() ) {
      omp_set_num_threads(omp_get_num_procs());
    } else {
      omp_set_num_threads(1);
    }
  } else if ( threads < 0 ) {
    omp_set_num_threads(omp_get_num_procs());
  } else {
    omp_set_num_threads(threads);
  }

  // normalise weights
  Py_BEGIN_ALLOW_THREADS
  ssim = 0.0;
  #pragma omp simd reduction(+:ssim)
  for ( npy_intp i = 0; i < PyArray_DIM(weights, 0); ++i ) {
    ssim += *(double *)PyArray_GETPTR1(weights, i);
  }
  ssim = 1.0 / ssim;
  #pragma omp simd
  for ( npy_intp i = 0; i < PyArray_DIM(weights, 0); ++i ) {
    *(double *)PyArray_GETPTR1(weights, i) *= ssim;
  }
  Py_END_ALLOW_THREADS

  PyArrayObject* kernel = gauss_kernel(3, 1.5);  // radius sigma

  // compute ssim
  switch ( PyArray_TYPE(im1) ) {
    case NPY_DOUBLE:
      Py_BEGIN_ALLOW_THREADS
      error = compute_ssim_float64(&ssim, im1, im2, weights, kernel);
      Py_END_ALLOW_THREADS
      break;
    default:
      PyErr_SetString(PyExc_TypeError, "only the types float64 are accepted");
      error = EXIT_FAILURE;
  }
  Py_DECREF(weights);
  Py_DECREF(kernel);

  // return and manage error
  if ( error == EXIT_FAILURE ) {
    return NULL;
  }
  return Py_BuildValue("d", ssim);
}


static PyObject* py_mse(PyObject* self, PyObject* args, PyObject* kwargs) {
  // declaration
  static char *kwlist[] = {"im1", "im2", "weights", "threads", NULL};
  PyArrayObject *im1, *im2, *weights = NULL;
  double mse;
  long int threads = 0;
  int error = EXIT_SUCCESS;

  // parse and check
  if ( !PyArg_ParseTupleAndKeywords(
    args, kwargs, "O!O!|O&$l", kwlist,
    &PyArray_Type, &im1, &PyArray_Type, &im2, &parse_float_array, &weights, &threads
    )
  ) {
    return NULL;
  }
  if ( PyArray_NDIM(im1) != 3 ) {
    PyErr_SetString(PyExc_ValueError, "'im1' requires 3 dimensions");
    return NULL;
  }
  if ( PyArray_NDIM(im2) != 3 ) {
    PyErr_SetString(PyExc_ValueError, "'im2' requires 3 dimensions");
    return NULL;
  }
  if ( PyArray_DIM(im1, 0) != PyArray_DIM(im2, 0) ) {
    PyErr_SetString(PyExc_ValueError, "'im1' and 'im2' must have the same height");
    return NULL;
  }
  if ( PyArray_DIM(im1, 1) != PyArray_DIM(im2, 1) ) {
    PyErr_SetString(PyExc_ValueError, "'im1' and 'im2' must have the same width");
    return NULL;
  }
  if ( PyArray_DIM(im1, 2) != PyArray_DIM(im2, 2) ) {
    PyErr_SetString(PyExc_ValueError, "'im1' and 'im2' must have the same channels");
    return NULL;
  }
  if ( PyArray_TYPE(im1) != PyArray_TYPE(im2) ) {
    PyErr_SetString(PyExc_TypeError, "'im1' and 'im2' are not the same type");
    return NULL;
  }

  // default values
  if ( weights == NULL ) {
    npy_intp dims[1] = {PyArray_DIM(im1, 2)};
    weights = (PyArrayObject *)PyArray_EMPTY(1, dims, NPY_DOUBLE, 0);
    if ( weights == NULL ) {
      PyErr_NoMemory();
      return NULL;
    }
    #pragma omp simd
    for ( npy_intp i = 0; i < PyArray_DIM(weights, 0); ++i ) {
      *(double *)PyArray_GETPTR1(weights, i) = 1.0;
    }
  }

  // set omp nbr threads
  if ( threads == 0 ) {
    if ( gettid() == getpid() ) {
      omp_set_num_threads(omp_get_num_procs());
    } else {
      omp_set_num_threads(1);
    }
  } else if ( threads < 0 ) {
    omp_set_num_threads(omp_get_num_procs());
  } else {
    omp_set_num_threads(threads);
  }

  // normalise weights
  Py_BEGIN_ALLOW_THREADS
  mse = 0.0;
  #pragma omp simd reduction(+:mse)
  for ( npy_intp i = 0; i < PyArray_DIM(weights, 0); ++i ) {
    mse += *(double *)PyArray_GETPTR1(weights, i);
  }
  mse = 1.0 / mse;
  #pragma omp simd
  for ( npy_intp i = 0; i < PyArray_DIM(weights, 0); ++i ) {
    *(double *)PyArray_GETPTR1(weights, i) *= mse;
  }
  Py_END_ALLOW_THREADS

  // compute mse
  switch ( PyArray_TYPE(im1) ) {
    case NPY_FLOAT32:
      Py_BEGIN_ALLOW_THREADS
      error = compute_mse_float32(&mse, im1, im2, weights);
      Py_END_ALLOW_THREADS
      break;
    case NPY_DOUBLE:
      Py_BEGIN_ALLOW_THREADS
      error = compute_mse_float64(&mse, im1, im2, weights);
      Py_END_ALLOW_THREADS
      break;
    default:
      PyErr_SetString(PyExc_TypeError, "only the types float32 and float64 are accepted");
      error = EXIT_FAILURE;
  }
  Py_DECREF(weights);

  // return and manage error
  if ( error == EXIT_FAILURE ) {
    return NULL;
  }
  return Py_BuildValue("d", mse);
}


static PyMethodDef metricMethods[] = {
  {
    "mse", (PyCFunction)py_mse, METH_VARARGS | METH_KEYWORDS,
    R"(Compute the mean square error of 2 images in C language.

    This function is nearly equivalent to:

    .. code-block:: python

        import numpy as np
        def mse(im1: np.ndarray, im2: np.ndarray, weights: list[float]) -> float:
            layers_mse = ((im1 - im2)**2).mean(axis=(0, 1)).tolist()
            tot = sum(weights)
            return sum(l*w/tot for w, l in zip(weights, layers_mse))

    Parameters
    ----------
    im1, im2 : np.ndarray
        The 2 images to be compared, of shape (height, width, channels).
        Supported formats are float32 and float64.
    threads : int, optional
        Defines the number of threads.
        The value -1 means that the function uses as many calculation threads as there are cores.
        The default value (0) allows the same behavior as (-1) if the function
        is called in the main thread, otherwise (1) to avoid nested threads.
        Any other positive value corresponds to the number of threads used.

    Returns
    -------
    mse : float
        The ponderated mean square error of each layers.

    Examples
    --------
    >>> import numpy as np
    >>> from cutcutcodec.core.filter.video.metric import mse
    >>> im1 = np.random.random((1080, 1920, 3))
    >>> im2 = np.random.random((1080, 1920, 3))
    >>> mse(im1, im2, [1.0, 2.0, 3.0])
    >>>
    )"
  },
  {
    "ssim", (PyCFunction)py_ssim, METH_VARARGS | METH_KEYWORDS,
    R"(Compute the Structural similarity index measure of 2 images in C language.

    This fonction is nearly equivalent to:

    .. code-block:: python

        from skimage.metrics import structural_similarity as ssim

    Parameters
    ----------
    im1, im2 : np.ndarray[np.float64]
        The 2 images to be compared, of shape (height, width, channels).
    threads : int, optional
        Defines the number of threads.
        The value -1 means that the function uses as many calculation threads as there are cores.
        The default value (0) allows the same behavior as (-1) if the function
        is called in the main thread, otherwise (1) to avoid nested threads.
        Any other positive value corresponds to the number of threads used.

    Returns
    -------
    ssim : float
        The ponderated structural similarity index measure of each layers.

    Examples
    --------
    >>> import numpy as np
    >>> from cutcutcodec.core.filter.video.metric import ssim
    >>> im1 = np.random.random((1080, 1920, 3))
    >>> im2 = np.random.random((1080, 1920, 3))
    >>> ssim(im1, im2, [1.0, 2.0, 3.0])
    >>>
    )"
  },
  {NULL, NULL, 0, NULL}
};


static struct PyModuleDef metric = {
  PyModuleDef_HEAD_INIT,
  "metric",
  "This module, implemented in C, offers functions for image metric calculation.",
  -1,
  metricMethods
};


PyMODINIT_FUNC PyInit_metric(void)
{
  import_array();
  if ( PyErr_Occurred() ) {
    return NULL;
  }
  return PyModule_Create(&metric);
}
