#!/usr/bin/env python3

"""Estimate the gain of the operations in the assembly graph.

This allows to choose whether or not to apply an operation,
depending on whether it should improve performance or not.
This module implements tools for deciding which operations to apply to the graph.
"""
