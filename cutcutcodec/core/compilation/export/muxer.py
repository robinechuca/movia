#!/usr/bin/env python3

"""Allow to suggest an appropriate muxer."""


def suggest_muxer():
    """Return the name of an ffmpeg container format appropriate for the given parameters."""
    return "matroska"
