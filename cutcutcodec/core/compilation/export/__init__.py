#!/usr/bin/env python3

"""Help to choose export parameters.

Indicates compatibility between muxers, codecs, encoders for video and audio parameters.
"""
