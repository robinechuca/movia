.. _api_reference:


Api Reference
=============

This is the definitive place to find all the details on CutCutCodec API documentation.


.. autosummary::
   :toctree: build/api
   :recursive:
   :template: custom_autosummary/module.rst

   cutcutcodec.core
   cutcutcodec.utils
