.. _installation:

Simple Installation
===================

For a more detailed and complete installation, please refer to :ref:`full_installation`.

Prerequisites
-------------

* It is preferable to install CutCutCodec in a virtual environment. More details on :ref:`virtual_environement`.
* Please install ``gcc``, ``ffmpeg`` and ``imagemagick``. More details on :ref:`dependencies`.

Basic Installation
------------------

To install cutcutcodec using `PyPI <https://pypi.org/project/cutcutcodec/>`_, use ``pip``:

.. code:: shell

    pip install cutcutcodec
