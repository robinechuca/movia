.. _documentation:

Build Documentation
===================

You can compile the documentation yourself after :ref:`full_installation`:

.. code:: shell

    cd docs/ && make clean && make html && cd -
    firefox docs/build/html/index.html


